import argparse

from criteria_calculation import CriteriaCalculation
from data_filtering import DataFiltering
from rank_ads import RankAds


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("extracted_ads_volume", type=str)
    parser.add_argument("--min_date", type=str)
    parser.add_argument("--max_date", type=str)
    parser.add_argument("--city_filter", type=str)
    parser.add_argument("--district", type=str)
    parser.add_argument("--min_price", type=int)
    parser.add_argument("--max_price", type=int)
    parser.add_argument("--min_size", type=int)
    parser.add_argument("--max_size", type=int)
    parser.add_argument("--min_rooms_no", type=int)
    parser.add_argument("--max_rooms_no", type=int)
    parser.add_argument("--floor_no", type=str)
    parser.add_argument("--market", type=str)
    parser.add_argument("--type_of_building", type=str)
    parser.add_argument("--building_material", type=str)
    parser.add_argument("--min_construction_year", type=int)
    parser.add_argument("--max_construction_year", type=int)
    parser.add_argument("--heating_type", type=str)
    parser.add_argument("--standard", type=str)
    parser.add_argument("--form_of_ownership", type=str)

    parser.add_argument("work_latitude", type=str)
    parser.add_argument("work_longitude", type=str)
    parser.add_argument("city", type=str)

    parser.add_argument("--distance_to_work_criterion_weight", type=float, default=1.0)
    parser.add_argument("--distance_to_lidl_criterion_weight", type=float, default=1.0)
    parser.add_argument("--distance_to_train_station_criterion_weight", type=float, default=1.0)
    parser.add_argument("--parks_no_criterion_weight", type=float, default=1.0)
    parser.add_argument("--supermarkets_no_criterion_weight", type=float, default=1.0)
    parser.add_argument("--distance_to_dance_studio_criterion_weight", type=float, default=1.0)
    parser.add_argument("--price_criterion_weight", type=float, default=1.0)

    args = parser.parse_args()

    data_filtering = DataFiltering(extracted_ads_volume=args.extracted_ads_volume, min_date=args.min_date,
                                   max_date=args.max_date, city=args.city_filter, district=args.district,
                                   min_price=args.min_price, max_price=args.max_price, min_size=args.min_size,
                                   max_size=args.max_size, min_rooms_no=args.min_rooms_no,
                                   max_rooms_no=args.max_rooms_no, floor_no=args.floor_no,
                                   market=args.market, type_of_building=args.type_of_building,
                                   building_material=args.building_material,
                                   min_construction_year=args.min_construction_year,
                                   max_construction_year=args.max_construction_year,
                                   heating_type=args.heating_type, standard=args.standard,
                                   form_of_ownership=args.form_of_ownership)

    ads_df = data_filtering.read_and_filter_data()

    criteria_calculation = CriteriaCalculation(ads_df, args.work_latitude, args.work_longitude, args.city)

    ads_df = criteria_calculation.calculate_criteria()

    rank_ads = RankAds(ads_df, args.distance_to_work_criterion_weight, args.distance_to_lidl_criterion_weight,
                       args.distance_to_train_station_criterion_weight, args.parks_no_criterion_weight,
                       args.supermarkets_no_criterion_weight, args.distance_to_dance_studio_criterion_weight,
                       args.price_criterion_weight)
    rank_ads.calculate_score()


if __name__ == "__main__":
    main()
