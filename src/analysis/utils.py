import geopy


def calculate_distance_to_nearest_object(ad, list_of_objects):
    flat_latitude = ad['latitude']
    flat_longitude = ad['longitude']
    flat_coordinates = (flat_latitude, flat_longitude)

    distances = []

    for i in range(0, len(list_of_objects)):
        object_latitude = list_of_objects[i]['lat']
        object_longitude = list_of_objects[i]['lon']
        object_coordinate = (object_latitude, object_longitude)
        distances.append(geopy.distance.geodesic(flat_coordinates, object_coordinate).km)

    return min(distances)


def calculate_objects_no_in_radius(ad, list_of_objects, radius):
    flat_latitude = ad['latitude']
    flat_longitude = ad['longitude']
    flat_coordinates = (flat_latitude, flat_longitude)

    objects_no = 0

    for i in range(0, len(list_of_objects)):
        object_latitude = list_of_objects[i]['lat']
        object_longitude = list_of_objects[i]['lon']
        object_coordinate = (object_latitude, object_longitude)
        if geopy.distance.geodesic(flat_coordinates, object_coordinate).km >= radius:
            objects_no += 1

    return objects_no
