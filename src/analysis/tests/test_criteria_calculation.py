import unittest
import pandas as pd

from analysis.criteria_calculation import CriteriaCalculation
import json


class TestCriteriaCalculation(unittest.TestCase):

    def test_replace_polish_signs(self):
        criteria_calculation = CriteriaCalculation(None, None, None, "Poznań")
        self.assertEqual("poznan", criteria_calculation.city)

    def test_calculate_distance_to_work(self):
        work_latitude = 51.109796
        work_longitude = 17.032742

        d = {'latitude': [51.112158, 51.111162], 'longitude': [17.021541, 17.031429]}
        ads = pd.DataFrame(data=d)

        ads_df = CriteriaCalculation(ads, work_latitude, work_longitude, 'wroclaw').calculate_criteria()

        self.assertAlmostEqual(0.82725, ads_df['distance_to_work'][0], places=5)
        self.assertAlmostEqual(0.17762, ads_df['distance_to_work'][1], places=5)

    def test_find_all_lidls_in_the_city(self):
        all_lidls = CriteriaCalculation.find_all_lidls_in_city("wroclaw")
        self.assertTrue(len(all_lidls) > 0)

    def test_calculate_distance_to_nearest_lidl(self):
        all_lidls = '[{"place_id":216301528,"licence":"Data © OpenStreetMap contributors, ODbL 1.0. https://osm.org/copyright","osm_type":"way","osm_id":647400519,"boundingbox":["51.1111471","51.1115688","17.0062471","17.0073276"],"lat":"51.11136065","lon":"17.006787526805425","display_name":"Lidl, 82, Braniborska, Szczepin, Osiedle Szczepin, Wroclaw, Wrocław, Lower Silesian Voivodeship, 53-680, Poland","class":"shop","type":"supermarket","importance":0.201,"icon":"https://nominatim.openstreetmap.org/images/mapicons/shopping_supermarket.p.20.png","address":{"shop":"Lidl","house_number":"82","road":"Braniborska","suburb":"Szczepin","city_district":"Osiedle Szczepin","city":"Wroclaw","municipality":"Wrocław","county":"Wrocław","state":"Lower Silesian Voivodeship","postcode":"53-680","country":"Poland","country_code":"pl"}},{"place_id":210312245,"licence":"Data © OpenStreetMap contributors, ODbL 1.0. https://osm.org/copyright","osm_type":"way","osm_id":612073597,"boundingbox":["51.1547905","51.1552679","16.8960945","16.8970458"],"lat":"51.1550196","lon":"16.896570177722587","display_name":"Lidl, 8, Daktylowa, Stabłowice Nowe, Leśnica, Osiedle Leśnica, Wroclaw, Wrocław, Lower Silesian Voivodeship, 54-054, Poland","class":"shop","type":"supermarket","importance":0.201,"icon":"https://nominatim.openstreetmap.org/images/mapicons/shopping_supermarket.p.20.png","address":{"shop":"Lidl","house_number":"8","road":"Daktylowa","neighbourhood":"Stabłowice Nowe","suburb":"Leśnica","city_district":"Osiedle Leśnica","city":"Wroclaw","municipality":"Wrocław","county":"Wrocław","state":"Lower Silesian Voivodeship","postcode":"54-054","country":"Poland","country_code":"pl"}}]'

        d = {'latitude': [51.112158, 51.111162], 'longitude': [17.021541, 17.031429]}
        ads = pd.DataFrame(data=d)

        criteria_calculation = CriteriaCalculation(ads, None, None, 'wroclaw')
        criteria_calculation.all_lidls = json.loads(all_lidls)
        ads['distance_to_nearest_lidl'] = ads.apply(criteria_calculation.calculate_distance_to_nearest_lidl, axis=1)

        self.assertAlmostEqual(1.03697, ads['distance_to_nearest_lidl'][0], places=5)
        self.assertAlmostEqual(1.72578, ads['distance_to_nearest_lidl'][1], places=5)

    def test_get_bounding_box_of_city(self):
        city = "wroclaw"
        bounding_box = CriteriaCalculation.get_bounding_box_of_city(city)
        self.assertEqual("(51.0426686,16.8073393,51.2100604,17.1762192)", bounding_box)

    def test_calculate_distance_to_nearest_tram_stop(self):
        all_tram_stops = '[{"type": "node","id": 173374079,"lat": 51.0960537,"lon": 16.9859749,"tags": {"name": "Hutmen","public_transport": "stop_position","railway": "tram_stop","ref": "11603","tram": "yes"}},{"type": "node","id": 265740436,"lat": 51.1048748,"lon": 17.0217876,"tags": {"name": "pl. Legionów","public_transport": "stop_position","railway": "tram_stop","ref": "10272","tram": "yes"}}]'

        d = {'latitude': [51.112158, 51.111162], 'longitude': [17.021541, 17.031429]}
        ads = pd.DataFrame(data=d)

        criteria_calculation = CriteriaCalculation(ads, None, None, 'wroclaw')
        criteria_calculation.all_train_stations = json.loads(all_tram_stops)
        ads['distance_to_nearest_train_station'] = ads.apply(
            criteria_calculation.calculate_distance_to_nearest_train_station, axis=1)

        self.assertAlmostEqual(0.81044, ads['distance_to_nearest_train_station'][0], places=5)
        self.assertAlmostEqual(0.97220, ads['distance_to_nearest_train_station'][1], places=5)

    def test_calculate_parks_no(self):
        all_parks = '[{"place_id":90440743,"licence":"Data © OpenStreetMap contributors, ODbL 1.0. https://osm.org/copyright","osm_type":"way","osm_id":24645888,"boundingbox":["51.1088236","51.1105853","17.0419904","17.047828"],"lat":"51.10966515","lon":"17.045352255338894","display_name":"Park Słowackiego, Ostrów Tumski, Osiedle Stare Miasto, Wroclaw, Wrocław, Lower Silesian Voivodeship, Poland","class":"leisure","type":"park","importance":0.39402874005523814,"address":{"leisure":"Park Słowackiego","quarter":"Ostrów Tumski","suburb":"Osiedle Stare Miasto","city":"Wroclaw","municipality":"Wrocław","county":"Wrocław","state":"Lower Silesian Voivodeship","country":"Poland","country_code":"pl"}},{"place_id":117781800,"licence":"Data © OpenStreetMap contributors, ODbL 1.0. https://osm.org/copyright","osm_type":"way","osm_id":128812093,"boundingbox":["51.1043919","51.1053626","17.0327519","17.0358507"],"lat":"51.104921399999995","lon":"17.034595392242636","display_name":"Park Mikołaja Kopernika, Dzielnica Czterech Świątyń, Osiedle Stare Miasto, Wroclaw, Wrocław, Lower Silesian Voivodeship, Poland","class":"leisure","type":"park","importance":0.38970188314703924,"address":{"leisure":"Park Mikołaja Kopernika","quarter":"Dzielnica Czterech Świątyń","suburb":"Osiedle Stare Miasto","city":"Wroclaw","municipality":"Wrocław","county":"Wrocław","state":"Lower Silesian Voivodeship","country":"Poland","country_code":"pl"}},{"place_id":97958282,"licence":"Data © OpenStreetMap contributors, ODbL 1.0. https://osm.org/copyright","osm_type":"way","osm_id":39473167,"boundingbox":["51.1193669","51.121916","17.0514996","17.0566337"],"lat":"51.12089","lon":"17.053247084450096","display_name":"Park Stanisława Tołpy, Ołbin, Osiedle Ołbin, Wroclaw, Wrocław, Lower Silesian Voivodeship, Poland","class":"leisure","type":"park","importance":0.3874189406975713,"address":{"leisure":"Park Stanisława Tołpy","suburb":"Ołbin","city_district":"Osiedle Ołbin","city":"Wroclaw","municipality":"Wrocław","county":"Wrocław","state":"Lower Silesian Voivodeship","country":"Poland","country_code":"pl"}},{"place_id":114593950,"licence":"Data © OpenStreetMap contributors, ODbL 1.0. https://osm.org/copyright","osm_type":"way","osm_id":120476934,"boundingbox":["51.118749","51.1195668","17.0423202","17.0453935"],"lat":"51.119173200000006","lon":"17.044030848445992","display_name":"Skwer Skaczącej Gwiazdy, Ołbin, Osiedle Ołbin, Wroclaw, Wrocław, Lower Silesian Voivodeship, 50-319, Poland","class":"leisure","type":"park","importance":0.38444095084610663,"address":{"leisure":"Skwer Skaczącej Gwiazdy","suburb":"Ołbin","city_district":"Osiedle Ołbin","city":"Wroclaw","municipality":"Wrocław","county":"Wrocław","state":"Lower Silesian Voivodeship","postcode":"50-319","country":"Poland","country_code":"pl"}}]'

        d = {'latitude': [51.112158, 51.111162], 'longitude': [17.021541, 17.031429]}
        ads = pd.DataFrame(data=d)

        criteria_calculation = CriteriaCalculation(ads, None, None, 'wroclaw')
        criteria_calculation.all_parks = json.loads(all_parks)
        ads['parks_no'] = ads.apply(
            criteria_calculation.calculate_parks_no, axis=1)

        self.assertEqual(1, ads['parks_no'][0])
        self.assertEqual(0, ads['parks_no'][1])

    def test_calculate_supermarkets_no(self):
        all_supermarkets = '[{"type": "node","id": 2721710574,"lat": 51.1037036,"lon": 17.0310974,"tags": {"addr:city": "Wrocław","addr:housenumber": "40","addr:postcode": "50-024","addr:street": "Świdnicka","brand": "Biedronka","email": "bok@biedronka.eu","level": "-1","name": "Biedronka","name:ru": "Бедронка","opening_hours": "Mo-Sa 07:00-21:00; Su 09:00-20:00","shop": "supermarket","start_date": "2017-12","website": "www.biedronka.pl","wheelchair": "yes"}},{"type": "node","id": 2721723040,"lat": 51.0994243,"lon": 17.0287660,"tags": {"addr:city": "Wrocław","addr:housenumber": "2-4","addr:street": "Powstańców Śląskich","contact:fax": "+48 71 334 14 11","contact:phone": "+48 71 334 14 00","level": "-1","name": "Eurospar","old_name": "Piotr i Paweł","opening_hours": "Mo-Sa 08:00-22:00; Su 09:00-21:00","shop": "supermarket"}},{"type": "node","id": 2721781314,"lat": 51.1085525,"lon": 17.0416782,"tags": {"brand": "Carrefour Market","brand:wikidata": "Q2689639","brand:wikipedia": "fr:Carrefour Market","level": "-1","name": "Carrefour Market","opening_hours": "Mo-Sa 09:30-21:00; Su 10:00-20:00","shop": "supermarket","wheelchair": "yes"}}]'

        d = {'latitude': [51.112158, 51.111162], 'longitude': [17.021541, 17.031429]}
        ads = pd.DataFrame(data=d)

        criteria_calculation = CriteriaCalculation(ads, None, None, 'wroclaw')
        criteria_calculation.all_supermarkets = json.loads(all_supermarkets)
        ads['supermarkets_no'] = ads.apply(
            criteria_calculation.calculate_supermarkets_no, axis=1)

        self.assertEqual(3, ads['supermarkets_no'][0])
        self.assertEqual(1, ads['supermarkets_no'][1])

    def test_calculate_distance_to_nearest_dance_studio(self):
        all_dance_studios = '[{"type": "node","id": 6372715605,"lat": 51.1104912,"lon": 17.0248065,"tags": {}},{"type": "node","id": 6372779185,"lat": 51.1082238,"lon": 17.0273327,"tags": {}},{"type": "node","id": 7093955272,"lat": 51.1105046,"lon": 17.0138100,"tags": {}}]'

        d = {'latitude': [51.112158, 51.111162], 'longitude': [17.021541, 17.031429]}
        ads = pd.DataFrame(data=d)

        criteria_calculation = CriteriaCalculation(ads, None, None, 'wroclaw')
        criteria_calculation.all_dance_studios = json.loads(all_dance_studios)
        ads['distance_to_nearest_dance_studio'] = ads.apply(
            criteria_calculation.calculate_distance_to_nearest_dance_studio, axis=1)

        self.assertAlmostEqual(0.29442, ads['distance_to_nearest_dance_studio'][0], places=5)
        self.assertAlmostEqual(0.43491, ads['distance_to_nearest_dance_studio'][1], places=5)


if __name__ == '__main__':
    unittest.main()
