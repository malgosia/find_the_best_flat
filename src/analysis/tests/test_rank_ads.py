import unittest
import pandas as pd

from analysis.rank_ads import RankAds


class TestRankAds(unittest.TestCase):

    def setUp(self):
        d = {'link': ['www.link1.com', 'www.link2.com', 'www.link3.com'],
             'price': [100000, 200000, 150000],
             'latitude': [51.112158, 51.111162, 51.109765],
             'longitude': [17.021541, 17.031429, 17.025678],
             'distance_to_work': [2.78941, 5.211213, 6.729438],
             'distance_to_nearest_lidl': [4.424324, 0.5324, 1.59480],
             'distance_to_nearest_train_station': [1.543534, 0.3000231, 9.45973],
             'parks_no': [3, 5, 7],
             'supermarkets_no': [1, 2, 3],
             'distance_to_nearest_dance_studio': [3.5435, 1.4234, 3.64353]
             }
        self.ads = pd.DataFrame(data=d)

    def test_normalize_weights_vector(self):
        normalized_array = RankAds.normalize_weights_vector(1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0)
        self.assertAlmostEqual(0.37796, normalized_array[1], places=5)

    def test_topsis_method(self):
        rank_ads = RankAds(self.ads)
        rank_ads.calculate_score()
        self.assertEqual('www.link2.com', rank_ads.ads['link'][0])


if __name__ == '__main__':
    unittest.main()
