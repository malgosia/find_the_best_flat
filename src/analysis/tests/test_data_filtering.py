import unittest

from analysis.data_filtering import DataFiltering


class TestDataFiltering(unittest.TestCase):

    def test_calculate_date_intervals_none_min_max_dates(self):
        today_date = "2020/06/01"
        min_date = None
        max_date = None
        self.assertEqual([today_date], DataFiltering.calculate_date_intervals(min_date, max_date, today_date))

    def test_calculate_date_intervals_none_min_date(self):
        today_date = "2020/05/31"
        min_date = None
        max_date = "2020/06/02"
        self.assertEqual(["2020/05/31", "2020/06/01", "2020/06/02"],
                         DataFiltering.calculate_date_intervals(min_date, max_date, today_date))

    def test_calculate_date_intervals_none_max_date(self):
        today_date = "2020/06/05"
        min_date = "2020/05/31"
        max_date = None
        self.assertEqual(["2020/05/31", "2020/06/01", "2020/06/02", "2020/06/03", "2020/06/04", "2020/06/05"],
                         DataFiltering.calculate_date_intervals(min_date, max_date, today_date))

    def test_calculate_date_intervals(self):
        today_date = "2020/06/05"
        min_date = "2020/05/31"
        max_date = "2020/06/05"
        self.assertEqual(["2020/05/31", "2020/06/01", "2020/06/02", "2020/06/03", "2020/06/04", "2020/06/05"],
                         DataFiltering.calculate_date_intervals(min_date, max_date, today_date))

    def test_read_and_filter_data(self):
        df = DataFiltering("data/extracted_ads", min_date="2020/06/12", max_date="2020/06/12", city='wrocław',
                           district='fabryczna', max_price=300000, min_rooms_no=2, max_rooms_no=2, market='wtórny',
                           type_of_building='blok').read_and_filter_data()
        self.assertEqual(1, df.shape[0])


if __name__ == '__main__':
    unittest.main()
