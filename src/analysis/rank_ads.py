import pandas as pd
import numpy as np
from topsis import topsis


class RankAds:

    def __init__(self, ads: pd.DataFrame, distance_to_work_criterion_weight=1.0,
                 distance_to_lidl_criterion_weight=1.0, distance_to_train_station_criterion_weight=1.0,
                 parks_no_criterion_weight=1.0, supermarkets_no_criterion_weight=1.0,
                 distance_to_dance_studio_criterion_weight=1.0, price_criterion_weight=1.0):
        self.ads = ads
        self.normalized_weights = self.normalize_weights_vector(distance_to_work_criterion_weight,
                                                                distance_to_lidl_criterion_weight,
                                                                distance_to_train_station_criterion_weight,
                                                                parks_no_criterion_weight,
                                                                supermarkets_no_criterion_weight,
                                                                distance_to_dance_studio_criterion_weight,
                                                                price_criterion_weight)

    def calculate_score(self):
        ads_df = self.ads[['distance_to_work', 'distance_to_nearest_lidl', 'distance_to_nearest_train_station',
                           'parks_no', 'supermarkets_no', 'distance_to_nearest_dance_studio', 'price']]
        decision_matrix = ads_df.to_numpy()
        benefit_cost_criteria = np.array([0, 0, 0, 1, 1, 0, 0])
        decision = topsis(decision_matrix, self.normalized_weights, benefit_cost_criteria)
        decision.calc()
        alternatives_ranking = decision.C
        self.ads['score'] = alternatives_ranking
        self.ads = self.ads.sort_values(by=['score'], ascending=False, ignore_index=True)
        with pd.option_context('display.max_rows', None, 'display.max_columns', None, 'display.encoding', 'UTF-8'):
            print(self.ads.to_string())

    @staticmethod
    def normalize_weights_vector(distance_to_work_criterion_weight, distance_to_lidl_criterion_weight,
                                 distance_to_train_station_criterion_weight, parks_no_criterion_weight,
                                 supermarkets_no_criterion_weight, distance_to_dance_studio_criterion_weight,
                                 price_criterion_weight):
        weights_array = np.array([distance_to_work_criterion_weight, distance_to_lidl_criterion_weight,
                                  distance_to_train_station_criterion_weight, parks_no_criterion_weight,
                                  supermarkets_no_criterion_weight, distance_to_dance_studio_criterion_weight,
                                  price_criterion_weight])
        return weights_array / np.sqrt(np.sum(weights_array ** 2))
