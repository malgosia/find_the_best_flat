import pandas as pd
import geopy.distance
import urllib.request
import json

from utils import calculate_distance_to_nearest_object, calculate_objects_no_in_radius


class CriteriaCalculation:

    def __init__(self, ads: pd.DataFrame, work_latitude, work_longitude, city):
        self.ads = ads
        self.work_latitude = work_latitude
        self.work_longitude = work_longitude
        self.city = city.lower().replace('ą', 'a').replace('ć', 'c').replace('ę', 'e').replace('ł', 'l').replace(
            'ń', 'n').replace('ó', 'o').replace('ś', 's').replace('ź', 'z').replace('ż', 'z')

        self.all_lidls = self.find_all_lidls_in_city(self.city)
        self.bounding_box = self.get_bounding_box_of_city(self.city)
        self.all_train_stations = self.find_all_train_stations(self.bounding_box)
        self.all_parks = self.find_all_parks(self.city)
        self.all_supermarkets = self.find_all_supermarkets(self.bounding_box)
        self.all_dance_studios = self.find_all_dance_studios(self.bounding_box)

    def calculate_criteria(self):
        self.ads['distance_to_work'] = self.ads.apply(self.calculate_distance_to_work, axis=1)
        self.ads['distance_to_nearest_lidl'] = self.ads.apply(self.calculate_distance_to_nearest_lidl, axis=1)
        self.ads['distance_to_nearest_train_station'] = self.ads.apply(self.calculate_distance_to_nearest_train_station,
                                                                       axis=1)
        self.ads['parks_no'] = self.ads.apply(self.calculate_parks_no, axis=1)
        self.ads['supermarkets_no'] = self.ads.apply(self.calculate_supermarkets_no, axis=1)
        self.ads['distance_to_nearest_dance_studio'] = self.ads.apply(self.calculate_distance_to_nearest_dance_studio,
                                                                      axis=1)
        return self.ads

    def calculate_distance_to_work(self, ad):
        flat_latitude = ad['latitude']
        flat_longitude = ad['longitude']
        flat_coordinates = (flat_latitude, flat_longitude)
        work_coordinates = (self.work_latitude, self.work_longitude)
        return geopy.distance.geodesic(flat_coordinates, work_coordinates).km

    @staticmethod
    def find_all_lidls_in_city(city):
        url = "https://nominatim.openstreetmap.org/?format=json&addressdetails=1&q=lidl+" + city + "&format=json&limit=100"
        resource = urllib.request.urlopen(url)
        return json.load(resource)

    def calculate_distance_to_nearest_lidl(self, ad):
        return calculate_distance_to_nearest_object(ad, self.all_lidls)

    @staticmethod
    def get_bounding_box_of_city(city):
        url = "https://nominatim.openstreetmap.org/search?city=" + city + "&format=json"
        resource = urllib.request.urlopen(url)
        bounding_box_json = json.load(resource)

        if len(bounding_box_json) == 0:
            raise Exception("No bounding box found.")

        bounding_box = bounding_box_json[0]["boundingbox"]
        bounding_box_str = "(" + bounding_box[0] + "," + bounding_box[2] + "," + bounding_box[1] + "," + bounding_box[
            3] + ")"
        return bounding_box_str

    @staticmethod
    def find_all_train_stations(bounding_box_of_city):
        url = "https://overpass-api.de/api/interpreter?data=[out:json];node[railway=tram_stop]" + bounding_box_of_city + ";out%20body;"
        resource = urllib.request.urlopen(url)
        return json.load(resource)['elements']

    def calculate_distance_to_nearest_train_station(self, ad):
        return calculate_distance_to_nearest_object(ad, self.all_train_stations)

    @staticmethod
    def find_all_parks(city):
        url = "https://nominatim.openstreetmap.org/?format=json&addressdetails=1&q=parks+" + city + "&format=json&limit=1000"
        resource = urllib.request.urlopen(url)
        return json.load(resource)

    def calculate_parks_no(self, ad):
        nearest_neighbourhood_radius = 2  # km
        return calculate_objects_no_in_radius(ad, self.all_parks, nearest_neighbourhood_radius)

    @staticmethod
    def find_all_supermarkets(bounding_box):
        url = "https://overpass-api.de/api/interpreter?data=[out:json];node[shop=supermarket]" + bounding_box + ";out%20body;"
        req = urllib.request.Request(url, headers={'User-Agent': 'Opera/9.80 (Windows NT 6.1; WOW64) Presto/2.12.388 Version/12.18'})
        resource = urllib.request.urlopen(req)
        return json.load(resource)['elements']

    def calculate_supermarkets_no(self, ad):
        nearest_neighbourhood_radius = 1  # km
        return calculate_objects_no_in_radius(ad, self.all_supermarkets, nearest_neighbourhood_radius)

    @staticmethod
    def find_all_dance_studios(bounding_box):
        url = "https://overpass-api.de/api/interpreter?data=[out:json];node[leisure=dance]" + bounding_box + ";out%20body;"
        req = urllib.request.Request(url, headers={'User-Agent': 'Mozilla/5.0 (X11; U; Linux i686) Gecko/20071127 Firefox/2.0.0.11'})
        resource = urllib.request.urlopen(req)
        return json.load(resource)['elements']

    def calculate_distance_to_nearest_dance_studio(self, ad):
        return calculate_distance_to_nearest_object(ad, self.all_dance_studios)
