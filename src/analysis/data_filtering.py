from datetime import date
import os
import pandas as pd


class DataFiltering:

    def __init__(self, extracted_ads_volume, min_date=None, max_date=None, city=None, district=None, min_price=None,
                 max_price=None, min_size=None, max_size=None, min_rooms_no=None, max_rooms_no=None, floor_no=None,
                 market=None, type_of_building=None, building_material=None, min_construction_year=None,
                 max_construction_year=None, heating_type=None, standard=None, form_of_ownership=None):
        self.extracted_ads_volume = extracted_ads_volume
        self.min_date = min_date
        self.max_date = max_date
        self.city = city
        self.district = district
        self.min_price = min_price
        self.max_price = max_price
        self.min_size = min_size
        self.max_size = max_size
        self.min_rooms_no = min_rooms_no
        self.max_rooms_no = max_rooms_no
        self.floor_no = floor_no
        self.market = market
        self.type_of_building = type_of_building
        self.building_material = building_material
        self.min_construction_year = min_construction_year
        self.max_construction_year = max_construction_year
        self.heating_type = heating_type
        self.standard = standard
        self.form_of_ownership = form_of_ownership

    @staticmethod
    def calculate_date_intervals(min_date, max_date, today_date):
        date_intervals = []
        if min_date is None and max_date is None:
            date_intervals.append(today_date)
        elif min_date is not None and max_date is None:
            date_intervals = [d.strftime('%Y/%m/%d') for d in pd.date_range(min_date, today_date).tolist()]
        elif min_date is None and max_date is not None:
            date_intervals = [d.strftime('%Y/%m/%d') for d in pd.date_range(today_date, max_date).tolist()]
        else:
            date_intervals = [d.strftime('%Y/%m/%d') for d in pd.date_range(min_date, max_date).tolist()]
        return date_intervals

    def read_and_filter_data(self):
        ads_df = self.read_ads_to_df()

        if self.min_price is not None:
            ads_df = ads_df.loc[ads_df['price'] >= self.min_price]

        if self.max_price is not None:
            ads_df = ads_df.loc[ads_df['price'] <= self.max_price]

        if self.min_size is not None:
            ads_df = ads_df.loc[ads_df['size'] >= self.min_size]

        if self.max_size is not None:
            ads_df = ads_df.loc[ads_df['size'] <= self.max_size]

        if self.min_rooms_no is not None:
            ads_df = ads_df.loc[ads_df['rooms_no'] >= self.min_rooms_no]

        if self.max_rooms_no is not None:
            ads_df = ads_df.loc[ads_df['rooms_no'] <= self.max_rooms_no]

        if self.floor_no is not None:
            ads_df = ads_df.loc[ads_df['floor_no'] == self.floor_no]

        if self.market is not None:
            ads_df = ads_df.loc[ads_df['market'] == self.market]

        if self.type_of_building is not None:
            ads_df = ads_df.loc[ads_df['type_of_building'] == self.type_of_building]

        if self.building_material is not None:
            ads_df = ads_df.loc[ads_df['building_material'] == self.building_material]

        if self.min_construction_year is not None:
            ads_df = ads_df.loc[ads_df['construction_year'] >= self.min_construction_year]

        if self.max_construction_year is not None:
            ads_df = ads_df.loc[ads_df['construction_year'] <= self.max_construction_year]

        if self.heating_type is not None:
            ads_df = ads_df.loc[ads_df['heating_type'] == self.heating_type]

        if self.standard is not None:
            ads_df = ads_df.loc[ads_df['standard'] == self.standard]

        if self.form_of_ownership is not None:
            ads_df = ads_df.loc[ads_df['form_of_ownership'] == self.form_of_ownership]

        return ads_df

    def read_ads_to_df(self):
        today_date = str(date.today()).replace("-", "/")
        date_intervals = self.calculate_date_intervals(self.min_date, self.max_date, today_date)

        ads_data = []

        for date_interval in date_intervals:
            path = os.path.join(self.extracted_ads_volume, date_interval)

            for ad_file in os.listdir(path):
                if (self.city is not None and self.district is None and ad_file.startswith(self.city.lower())) \
                        or (
                        self.district is not None and ad_file == self.city.lower() + "-" + self.district.lower() + ".csv") \
                        or (self.city is None and self.district is None):
                    ads_data.append(pd.read_csv(os.path.join(path, ad_file), header=None, sep=";", encoding="utf-8"))

        if len(ads_data) == 0:
            raise Exception("No data found.")

        ads_df = pd.concat(ads_data, ignore_index=True)
        ads_df.columns = ['link', 'price', 'address', 'size', 'rooms_no', 'floor_no', 'all_floor_no', 'market',
                          'type_of_building', 'building_material', 'construction_year', 'heating_type', 'standard',
                          'form_of_ownership', 'latitude', 'longitude']

        return ads_df
