import argparse
from scrapy.crawler import CrawlerProcess

from ads_crawler import AdsCrawler

default_base_url = "https://www.otodom.pl/pl/wyniki/sprzedaz/mieszkanie/dolnoslaskie/wroclaw/wroclaw/wroclaw?page="


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("ads_ids_volume", type=str)
    parser.add_argument("ads_volume", type=str)
    parser.add_argument("--max_pages_no", type=int, default=50)
    parser.add_argument("--base_url", type=str, default=default_base_url)

    args = parser.parse_args()

    process = CrawlerProcess({
        'USER_AGENT': 'Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1)'
    })
    process.crawl(AdsCrawler,
                  ads_ids_volume=args.ads_ids_volume,
                  ads_volume=args.ads_volume,
                  max_pages_no=args.max_pages_no,
                  base_url=args.base_url)
    process.start()


if __name__ == "__main__":
    main()
