import unittest
import os
import shutil

from scrapy.crawler import CrawlerProcess
from crawler.ads_crawler import AdsCrawler


class TestCrawler(unittest.TestCase):

    def test_prepare_urls(self):
        actual_urls = AdsCrawler.prepare_urls("www.otodom.pl", 20)
        self.assertEqual(20, len(actual_urls))
        self.assertEqual("3", actual_urls[2][-1])

    def test_extract_ad_id(self):
        http_address = 'https://www.otodom.pl/oferta/centrum-4-pokoje-z-duzym-balkonem-ID460rD.html#676f7220f2'
        actual_ad_id = AdsCrawler.extract_ad_id(http_address)
        self.assertEqual('centrum-4-pokoje-z-duzym-balkonem-ID460rD', actual_ad_id)

    def test_extract_city_and_district_name(self):
        address = "Wroclaw, Popowice, Sloneczna"
        actual_city_district_name = AdsCrawler.extract_city_and_district_name(address)
        self.assertEqual('wroclaw-popowice', actual_city_district_name)

    def test_extract_city_and_district_name_no_city(self):
        address = ""
        self.assertRaises(ValueError, AdsCrawler.extract_city_and_district_name, address)

    def test_extract_city_and_district_name_city_and_district(self):
        address = "Wrocław, Krzyki, Gaj, ok. Uniwersyteckiego Szpitala Klinicznego"
        actual_city_district_name = AdsCrawler.extract_city_and_district_name(address)
        self.assertEqual('wroclaw-krzyki', actual_city_district_name)

    def test_current_datetime(self):
        current_date = AdsCrawler.get_current_date()
        self.assertEqual("/", current_date[4])
        self.assertEqual("/", current_date[7])

    def test_crawler_process(self):
        current_dir = os.getcwd()
        os.makedirs(os.path.join(current_dir, "data/ads_ids_volume"))
        os.makedirs(os.path.join(current_dir, "data/ads_volume"))
        process = CrawlerProcess({
            'USER_AGENT': 'Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1)'
        })
        process.crawl(AdsCrawler, ads_ids_volume=current_dir + "/data/ads_ids_volume",
                      ads_volume=current_dir + "/data/ads_volume")
        process.start()
        shutil.rmtree(os.path.join(current_dir, "data/ads_ids_volume"))
        shutil.rmtree(os.path.join(current_dir, "data/ads_volume"))
        shutil.rmtree(os.path.join(current_dir, "data"))


if __name__ == '__main__':
    unittest.main()
