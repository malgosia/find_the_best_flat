from datetime import date
import os
from scrapy import Request
from scrapy.spiders import CrawlSpider


class AdsCrawler(CrawlSpider):
    name = "OtoDomCrawler"

    default_base_url = "https://www.otodom.pl/pl/wyniki/sprzedaz/mieszkanie/dolnoslaskie/wroclaw/wroclaw/wroclaw?page="

    def __init__(self, ads_ids_volume, ads_volume, max_pages_no=5, base_url=default_base_url):
        self.ads_ids_volume = ads_ids_volume
        self.ads_volume = ads_volume
        self.max_pages_no = max_pages_no
        self.base_url = base_url
        self.default_otodom_url = "https://www.otodom.pl"

    def start_requests(self):
        urls = self.prepare_urls(self.base_url, self.max_pages_no)

        for url in urls:
            yield Request(url=url, callback=self.parse)

    def parse(self, response):
        ads_urls = self.extract_ads_ids(response)
        addresses = self.extract_addresses(response)

        for ad_url, address in zip(ads_urls, addresses):
            extracted_id = self.extract_ad_id(ad_url)
            if not self.file_exists(self.ads_ids_volume, extracted_id):
                self.create_empty_file(self.ads_ids_volume, extracted_id)
                yield Request(url=self.default_otodom_url + ad_url, callback=self.extract_ad_content_and_save, cb_kwargs=dict(address=address))

    def extract_ad_content_and_save(self, response, address):
        ad_content = response.text.replace("\n", "").replace("\r", "") + "\n"
        city_and_district = self.extract_city_and_district_name(address)
        current_date = self.get_current_date()

        if not os.path.exists(os.path.join(self.ads_volume, current_date)):
            os.makedirs(os.path.join(self.ads_volume, current_date))

        with open(os.path.join(self.ads_volume, os.path.join(current_date, city_and_district + ".mhtml")), "a",
                  encoding="utf-8") as f:
            f.write(ad_content)
            f.close()

    @staticmethod
    def get_current_date():
        return str(date.today()).replace("-", "/")

    @staticmethod
    def extract_city_and_district_name(address):
        address_splitted = address.split(",")
        address_len = len(address_splitted)
        if len(address_splitted[0]) == 0:
            raise ValueError("Address does not contain city name")
        city = address_splitted[address_len-2]
        district = address_splitted[address_len-3]
        return (city + "-" + district).lower().replace("/", "").replace(".", "").replace(" ", "") \
            .replace('ą', 'a').replace('ć', 'c').replace('ę', 'e').replace('ł', 'l').replace('ń', 'n') \
            .replace('ó', 'o').replace('ś', 's').replace('ź', 'z').replace('ż', 'z')

    @staticmethod
    def file_exists(directory, file_name):
        return os.path.exists(os.path.join(directory, file_name))

    @staticmethod
    def create_empty_file(directory, file_name):
        with open(os.path.join(directory, file_name), "w") as f:
            f.close()

    @staticmethod
    def extract_ad_id(http_address):
        splitted_address = http_address.split("/")
        return splitted_address[len(splitted_address) - 1]

    @staticmethod
    def extract_ads_ids(response):
        return response.xpath('//div[@data-cy=\'search.listing.organic\']/ul[1]/li/a/@href').extract()

    @staticmethod
    def extract_addresses(response):
        return response.xpath('//div[@data-cy=\'search.listing.organic\']/ul[1]/li/a/article/p/text()').extract()

    @staticmethod
    def prepare_urls(base_url, max_pages_no):
        return [base_url + str(i) for i in range(1, max_pages_no + 1)]
