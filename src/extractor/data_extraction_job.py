import argparse

from data_extraction import DataExtraction


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("ads_volume", type=str)
    parser.add_argument("extraction_date", type=str)
    parser.add_argument("extracted_ads_volume", type=str)

    args = parser.parse_args()

    extractor = DataExtraction(ads_volume=args.ads_volume, extraction_date=args.extraction_date,
                               extracted_ads_volume=args.extracted_ads_volume)
    extractor.extract_ads()


if __name__ == "__main__":
    main()
