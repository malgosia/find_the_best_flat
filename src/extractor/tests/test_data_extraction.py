import unittest

from extractor.data_extraction import DataExtraction


class TestDataExtraction(unittest.TestCase):

    def test_extract_necessary_data(self):
        extracted_ads = []
        with open("example_ad.mhtml", "r") as f:
            for _, line in enumerate(f):
                extracted_ads.append(DataExtraction(ads_volume="/data", extraction_date="2021/02/21",
                                                    extracted_ads_volume="/data/extracted_ads").extract_necessary_data(
                    line))
        self.assertEqual("https://www.otodom.pl/pl/oferta/wroclaw-stare-miasto-szczepin-ID49TLZ.html",
                         extracted_ads[0].split(";")[0])
        self.assertEqual("wtórny", extracted_ads[0].split(";")[7])
        self.assertEqual("https://www.otodom.pl/pl/oferta/lokum-porto-nowe-mieszkanie-a1-0-2-ID49noM.html",
                         extracted_ads[2].split(";")[0])
        self.assertEqual("43.92", extracted_ads[2].split(";")[3])
        self.assertEqual("", extracted_ads[2].split(";")[9])
        self.assertEqual("2022", extracted_ads[2].split(";")[10])
        self.assertEqual("", extracted_ads[2].split(";")[12])
        self.assertEqual("51.12527403408857", extracted_ads[2].split(";")[14])
        self.assertEqual("17.018101241833495", extracted_ads[2].split(";")[15])

    def test_get_attribute(self):
        ad_characteristics = [
          {
            "key": "price",
            "value": "470000",
            "label": "Cena",
            "localizedValue": "470 000 zł",
            "currency": "PLN",
            "suffix": "",
            "__typename": "Characteristic"
          },
          {
            "key": "m",
            "value": "57",
            "label": "Powierzchnia",
            "localizedValue": "57 m²",
            "currency": "",
            "suffix": "",
            "__typename": "Characteristic"
          },
          {
            "key": "price_per_m",
            "value": "8246",
            "label": "cena za metr",
            "localizedValue": "8 246 zł/m²",
            "currency": "",
            "suffix": "",
            "__typename": "Characteristic"
          },
          {
            "key": "rooms_num",
            "value": "3",
            "label": "Liczba pokoi",
            "localizedValue": "3",
            "currency": "",
            "suffix": "",
            "__typename": "Characteristic"
          },
          {
            "key": "market",
            "value": "secondary",
            "label": "Rynek",
            "localizedValue": "wtórny",
            "currency": "",
            "suffix": "",
            "__typename": "Characteristic"
          },
          {
            "key": "building_type",
            "value": "block",
            "label": "Rodzaj zabudowy",
            "localizedValue": "blok",
            "currency": "",
            "suffix": "",
            "__typename": "Characteristic"
          },
          {
            "key": "floor_no",
            "value": "floor_4",
            "label": "Piętro",
            "localizedValue": "4",
            "currency": "",
            "suffix": "",
            "__typename": "Characteristic"
          },
          {
            "key": "building_floors_num",
            "value": "10",
            "label": "Liczba pięter",
            "localizedValue": "10",
            "currency": "",
            "suffix": "",
            "__typename": "Characteristic"
          },
          {
            "key": "heating",
            "value": "urban",
            "label": "Ogrzewanie",
            "localizedValue": "miejskie",
            "currency": "",
            "suffix": "",
            "__typename": "Characteristic"
          },
          {
            "key": "build_year",
            "value": "1988",
            "label": "Rok budowy",
            "localizedValue": "1988",
            "currency": "",
            "suffix": "",
            "__typename": "Characteristic"
          },
          {
            "key": "construction_status",
            "value": "ready_to_use",
            "label": "Stan wykończenia",
            "localizedValue": "do zamieszkania",
            "currency": "",
            "suffix": "",
            "__typename": "Characteristic"
          },
          {
            "key": "building_ownership",
            "value": "full_ownership",
            "label": "Forma własności",
            "localizedValue": "pełna własność",
            "currency": "",
            "suffix": "",
            "__typename": "Characteristic"
          }
        ]

        self.assertEqual("3", DataExtraction.get_attribute(ad_characteristics, "Liczba pokoi"))
        self.assertEqual("blok", DataExtraction.get_attribute(ad_characteristics, "Rodzaj zabudowy"))
