import json
import os
import shutil
from scrapy.selector import Selector


class DataExtraction:

    def __init__(self, ads_volume, extraction_date, extracted_ads_volume):
        self.ads_volume = ads_volume
        self.extraction_date = extraction_date.replace("-", "/")
        self.extracted_ads_volume = extracted_ads_volume

    def extract_ads(self):
        ads_path = os.path.join(self.ads_volume, self.extraction_date)
        extracted_ads_path = os.path.join(self.extracted_ads_volume, self.extraction_date)

        if os.path.isdir(extracted_ads_path):
            shutil.rmtree(extracted_ads_path, ignore_errors=True)

        os.makedirs(extracted_ads_path)

        for ad_mhtml_file in os.listdir(ads_path):
            extracted_ads = []
            with open(os.path.join(ads_path, ad_mhtml_file), "r", encoding="utf-8") as f:
                for _, ad_content in enumerate(f):
                    extracted_ads.append(self.extract_necessary_data(ad_content))
                f.close()
            with open(os.path.join(extracted_ads_path, ad_mhtml_file.replace("mhtml", "csv")), "w", encoding="utf-8") as f:
                for extracted_ad in extracted_ads:
                    f.write(extracted_ad + "\n")
                f.close()

    def extract_necessary_data(self, ad_content):
        selector = Selector(text=ad_content)
        json_object = json.loads(selector.xpath('//*[@id="__NEXT_DATA__"]/text()').get())
        ad = json_object['props']['pageProps']['ad']
        link = ad['url']
        price = str(ad['target']['Price'])
        address = ad['location']['address'][0]['value']

        attributes = ad['characteristics']

        size = str(ad['target']['Area'])
        rooms_no = str(self.get_attribute(attributes, "Liczba pokoi"))
        floor_no = str(self.get_attribute(attributes, "Piętro"))
        all_floor_no = str(self.get_attribute(attributes, "Liczba pięter"))
        market = self.get_attribute(attributes, "Rynek")
        type_of_building = self.get_attribute(attributes, "Rodzaj zabudowy")
        building_material = self.get_attribute(attributes, "Materiał budynku")
        construction_year = str(self.get_attribute(attributes, "Rok budowy"))
        heating_type = self.get_attribute(attributes, "Ogrzewanie")
        standard = self.get_attribute(attributes, "Stan wykończenia")
        form_of_ownership = self.get_attribute(attributes, "Forma własności")

        latitude = str(ad['location']['coordinates']['latitude'])
        longitude = str(ad['location']['coordinates']['longitude'])

        properties = [link, price, address, size, rooms_no, floor_no, all_floor_no, market, type_of_building,
                      building_material, construction_year, heating_type, standard, form_of_ownership, latitude,
                      longitude]
        separator = ";"
        return separator.join(properties)

    @staticmethod
    def get_attribute(ad_characteristics, attribute_name):
        for characteristic in ad_characteristics:
            if characteristic['label'] == attribute_name:
                return characteristic['localizedValue']
        return ""
