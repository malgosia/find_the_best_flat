# Find the best flat

Find the best flat is an application containing 3 parts:

* crawler
* extractor
* analysis

# Crawler
Crawler is responsible for downloading ads from www.otodom.pl service.
The default crawler is looking for flats in Wrocław.

Required parameters:
* **ads_ids_volume** - path to storage where ids of ads will be put
* **ads_volume** - path to storage where ads will be put

Optional parameters:
* **max_pages_no** - how many pages should crawler visit (default is 5)
* **base_url** - default is "https://www.otodom.pl/sprzedaz/mieszkanie/wroclaw/?search%5Bregion_id%5D=1&search%5Bsubregion_id%5D=381&search%5Bcity_id%5D=39&page="

The crawler visits the page and extracts all links to ads.
If the ad id exists in ads ids volume, the ad is skipped. 
Otherwise the ad id is saved in ads ids volume and the content of the ad 
is appended to file in proper directory of ads volume.
The directory consists of current date (YYYY/MM/DD). The file name is city-district.mhtml. 


# Extractor
Extractor is responsible for finding the most important attributes of the ad (f.e. price, size, coordinates) and saving them to CSV files.

Required parameters:
* **ads_volume** - path to storage with ads
* **extraction_date** - from which directory in ads_volume the ads should be extracted
* **extracted_ads_volume** - path to storage where extracted ads will be put

Extractor reads all the files from proper directory in ads_volume storage, f.e.:
```
ads_volume/2021/03/04


-rw-r--r-- 1 root root  870603 mar  4 18:03 wroclaw-dolnoslaskie.mhtml
-rw-r--r-- 1 root root 6597398 mar  4 18:03 wroclaw-fabryczna.mhtml
-rw-r--r-- 1 root root 6099364 mar  4 18:03 wroclaw-krzyki.mhtml
-rw-r--r-- 1 root root  352987 mar  4 18:03 wroclaw-nadodrze.mhtml
-rw-r--r-- 1 root root 5104131 mar  4 18:03 wroclaw-psiepole.mhtml
-rw-r--r-- 1 root root  333524 mar  4 18:03 wroclaw-rynek.mhtml
-rw-r--r-- 1 root root 1987271 mar  4 18:03 wroclaw-srodmiescie.mhtml
-rw-r--r-- 1 root root 1941665 mar  4 18:03 wroclaw-staremiasto.mhtml
```

The application extract the necessary data from the content of mhtml files:

* link 
* price
* address
* size
* rooms_no
* floor_no
* all_floor_no
* market
* type_of_building
* building_material
* construction_year
* heating_type
* standard
* form_of_ownership
* latitude
* longitude

The data is saved to extracted_ads_volume with the following structure:
```
extracted_ads_volume/2020/06/15


-rw-r--r-- 1 root root  992 mar  4 18:05 wroclaw-dolnoslaskie.csv
-rw-r--r-- 1 root root 7534 mar  4 18:05 wroclaw-fabryczna.csv
-rw-r--r-- 1 root root 7049 mar  4 18:05 wroclaw-krzyki.csv
-rw-r--r-- 1 root root  384 mar  4 18:05 wroclaw-nadodrze.csv
-rw-r--r-- 1 root root 5903 mar  4 18:05 wroclaw-psiepole.csv
-rw-r--r-- 1 root root  422 mar  4 18:05 wroclaw-rynek.csv
-rw-r--r-- 1 root root 2385 mar  4 18:05 wroclaw-srodmiescie.csv
-rw-r--r-- 1 root root 2440 mar  4 18:05 wroclaw-staremiasto.csv

```

# Analysis

The last part of the **find the best flat** application is ads analysis. This part is responsible for finding the best flat in a given city.

First, the data is filtered. User can specify the following filters:
* **min_date**
* **max_date**
* **city**
* **district**
* **min_price**
* **max_price**
* **min_size**
* **max_size**
* **min_rooms_no**
* **max_rooms_no**
* **floor_no**
* **market**
* **type_of_building**
* **building_material**
* **min_construction_year**
* **max_construction_year**
* **heating_type**
* **standard**
* **form_of_ownership**

There is one required parameter: path to **extracted_ads_volume**.

The second step is to calculate criteria. Required parameters in this step:

* **ads**: pd.DataFrame (calculated in data filtering step)
* **work_latitude** - latitude of workplace coordinate
* **work_longitude** - longitude of workplace coordinate
* **city** - name of the city

Using nominatim and overpass api the most important objects in the city are found (f.e. parks, supermarkets, tram stops etc.)

The following criteria are calculated:
* **distance_to_work** - distance to (work_latitude, work_longitude)
* **distance_to_nearest_lidl** - distance to nearest Lidl supermarket found by Nominatim/Overpass API
* **distance_to_nearest_train_station** - distance to nearest tram stop
* **parks_no** - number of parks within 2 km radius
* **supermarkets_no** - number of supermarkets within 1 km radius
* **distance_to_nearest_dance_studio** - distance to nearest dance studio

The last step uses TOPSIS method to rank the ads. Using criteria and their weights, the ads are scored and sorted.
User can specify the following weights (default values are equal to **1.0**):
* **distance_to_work_criterion_weight**
* **distance_to_lidl_criterion_weight**
* **distance_to_train_station_criterion_weight**
* **parks_no_criterion_weight**
* **supermarkets_no_criterion_weight**
* **distance_to_dance_studio_criterion_weight**
* **price_criterion_weight**


# Deployment

Use Dockerfiles attached for each application:
* for crawler:
```
docker build . -t find_the_best_flat_crawler:0.1
```
* for extractor:
```
docker build . -t find_the_best_flat_extractor:0.1
```
* for analysis:
```
docker build . -t find_the_best_flat_analysis:0.1
```

Run the container:

* crawler job run example:

```
docker run -v /home/data/ids:/home/data/ids -v /home/data:/home/data find_the_best_flat_crawler:0.1 /home/data/ids /home/data
```
* extractor job example:
```
docker run -v /home/data/extracted_ads:/home/data/extracted_ads -v /home/data:/home/data find_the_best_flat_extractor:0.1 /home/data 2021/03/02 /home/data/extracted_ads
```
* analysis job example:
```
docker run -v /home/data/extracted_ads:/home/data/extracted_ads --dns 8.8.8.8 find_the_best_flat_analysis:0.1 /home/data/extracted_ads 51.112 17.0543 wroclaw
```

Passing optional arguments:
```
docker run -v /home/data/extracted_ads:/home/data/extracted_ads --dns 8.8.8.8 find_the_best_flat_analysis:0.1 /home/data/extracted_ads 51.112 17.0543 wroclaw --floor_no=1 --min_date=2021/03/02 --max_date=2021/03/02
```