variable "ssh_public_key" {
    default = "~/.ssh/id_rsa.pub"
}

variable "name" {
    default = "findthebestflat"
}