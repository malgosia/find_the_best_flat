output "ACR_HOST" {
    value = azurerm_container_registry.acr.login_server
}

output "ACR_NAME" {
    value = azurerm_container_registry.acr.name
}