terraform {
    required_providers {
        azurerm = {
            source = "hashicorp/azurerm"
            version = "=2.46.0"
        }
    }
}

resource "azurerm_resource_group" "k8s" {
    name     = var.name
    location = "eastus2"
}

resource "random_id" "random" {
  byte_length = 6
}

resource "azurerm_container_registry" "acr" {
  name                     = "${var.name}${random_id.random.hex}"
  resource_group_name      = azurerm_resource_group.k8s.name
  location                 = azurerm_resource_group.k8s.location
  sku                      = "Basic"
}

resource "azurerm_kubernetes_cluster" "k8s" {
    name                = var.name
    location            = azurerm_resource_group.k8s.location
    resource_group_name = azurerm_resource_group.k8s.name
    dns_prefix          = var.name

    kubernetes_version  = "1.20.2"

    linux_profile {
        admin_username = "ubuntu"

        ssh_key {
            key_data = file(var.ssh_public_key)
        }
    }

    network_profile {
        load_balancer_sku = "Basic"
        network_plugin = "kubenet"
        network_policy = "calico"
    }

    default_node_pool {
        name            = "agentpool"
        node_count      = 3
        vm_size         = "Standard_E2s_v3"
    }

    identity {
        type = "SystemAssigned"
    }

    role_based_access_control {
        enabled = true
    }

    tags = {
        Environment = "findthebestflat"
    }
}

resource "azurerm_role_assignment" "acr" {
  scope                = azurerm_container_registry.acr.id
  role_definition_name = "AcrPull"
  principal_id         = azurerm_kubernetes_cluster.k8s.kubelet_identity[0].object_id
}

resource "azurerm_storage_account" "k8s" {
  name                     = "azurefindflatstorage"
  resource_group_name      = azurerm_resource_group.k8s.name
  location                 = azurerm_resource_group.k8s.location
  account_tier             = "Standard"
  account_replication_type = "LRS"
}

resource "azurerm_storage_share" "k8s" {
  name                 = "azurefindlatstshare"
  storage_account_name = azurerm_storage_account.k8s.name
  quota                = 50
}

resource "azurerm_storage_share_directory" "ads" {
  name                 = "ads"
  share_name           = azurerm_storage_share.k8s.name
  storage_account_name = azurerm_storage_account.k8s.name
}

resource "azurerm_storage_share_directory" "adsids" {
  name                 = "adsids"
  share_name           = azurerm_storage_share.k8s.name
  storage_account_name = azurerm_storage_account.k8s.name
}

resource "azurerm_storage_share_directory" "extractedads" {
  name                 = "extractedads"
  share_name           = azurerm_storage_share.k8s.name
  storage_account_name = azurerm_storage_account.k8s.name
}

resource "azurerm_storage_share" "airflow" {
  name                 = "azureairflow"
  storage_account_name = azurerm_storage_account.k8s.name
  quota                = 50
}

resource "azurerm_storage_share_directory" "airflowdags" {
  name                 = "airflowdags"
  share_name           = azurerm_storage_share.airflow.name
  storage_account_name = azurerm_storage_account.k8s.name
}

resource "azurerm_storage_share_directory" "airflowlogs" {
  name                 = "airflowlogs"
  share_name           = azurerm_storage_share.airflow.name
  storage_account_name = azurerm_storage_account.k8s.name
}