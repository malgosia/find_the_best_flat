# Deployment on Azure

# Prerequisites
Install:
* Azure CLI
* docker
* kubectl
* terraform
* helm

# Install Azure Kubernetes Service
Generate your SSH Public Key:
```
ssh-keygen
```

Log to Azure account:
```
az login
```

Check your Azure account:
```
az account show
```

Deploy Azure Kubernetes Cluster:
```
cd terraform
terraform init
terraform plan
terraform apply
```

If the resources must be deleted:
```
terraform destroy
```

# Push Docker images to Azure Container Registry
Export Azure Kubernetes config to local machine:

```
az aks get-credentials -g findthebestflat -n findthebestflat
```
It will export config to ~/.kube/config.

Use kubectl command:
```
kubectl get namespaces
```

Create namespace:
```
kubectl create namespace find-the-best-flat
```

Check terraform output:
```
terraform output
```
You will receive:
```
ACR_HOST = "findthebestflat<RANDOM_ID>.azurecr.io"
ACR_NAME = "findthebestflat<RANDOM_ID>"
```

Login to Azure Container Registry:
```
az acr login --name ACR_NAME
```
where ACR_NAME is name from terraform output.

Tag the Docker images using the following command:

```
docker tag image_name:image_tag ACR_HOST/find_the_best_flat/image_name:image_tag
```
Examples:
```
docker tag find_the_best_flat_crawler:0.1 ACR_HOST/find_the_best_flat/find_the_best_flat_crawler:0.1
docker tag find_the_best_flat_extractor:0.1 ACR_HOST/find_the_best_flat/find_the_best_flat_extractor:0.1
docker tag find_the_best_flat_analysis:0.1 ACR_HOST/find_the_best_flat/find_the_best_flat_analysis:0.1
```

Push the Docker images to Azure Container Registry:
```
docker push ACR_HOST/find_the_best_flat/find_the_best_flat_crawler:0.1
docker push ACR_HOST/find_the_best_flat/find_the_best_flat_extractor:0.1
docker push ACR_HOST/find_the_best_flat/find_the_best_flat_analysis:0.1
```

# Create PVs and PVCs:

Check storage account name

```
AKS_PERS_STORAGE_ACCOUNT_NAME="azurefindflatstorage" (+RANDOM)
```

Get storage account key
```
STORAGE_KEY=$(az storage account keys list --resource-group $AKS_PERS_RESOURCE_GROUP --account-name $AKS_PERS_STORAGE_ACCOUNT_NAME --query "[0].value" -o tsv)
```

Create Kubernetes secret (name = azure-account-secret):
```
kubectl create secret generic azure-account-secret --from-literal=azurestorageaccountname=$AKS_PERS_STORAGE_ACCOUNT_NAME --from-literal=azurestorageaccountkey=$STORAGE_KEY
```
Create PV and PVCs using the following commands:
```
kubectl apply -f ads_ids_pv.yml
kubectl apply -f ads_ids_pvc.yml
kubectl apply -f ads_pv.yml
kubectl apply -f ads_pvc.yml
kubectl apply -f extracted_ads_pv.yml
kubectl apply -f extracted_ads_pvc.yml
```
Check created PVs:
```
kubectl get pv
```
Output:
```
NAME           CAPACITY   ACCESS MODES   RECLAIM POLICY   STATUS   CLAIM                             STORAGECLASS   REASON   AGE
ads            5Gi        RWX            Retain           Bound    find-the-best-flat/ads                                    5m31s
adsids         1Gi        RWX            Retain           Bound    find-the-best-flat/adsids                                 11m
extractedads   2Gi        RWX            Retain           Bound    find-the-best-flat/extractedads                           4m46s
```
Check created PVCs:
```
kubectl get pvc -n find-the-best-flat
```
Output:
```
NAME           STATUS   VOLUME         CAPACITY   ACCESS MODES   STORAGECLASS   AGE
ads            Bound    ads            5Gi        RWX                           6m51s
adsids         Bound    adsids         1Gi        RWX                           13m
extractedads   Bound    extractedads   2Gi        RWX                           6m9s
```
# Create pods:

Change ACR_HOST in pods definitions and run them using the following commands:
```
kubectl apply -f crawler_pod.yml
kubectl apply -f extractor_pod.yml
kubectl apply -f analysis_pod.yml
```
Pods definitions are similar. They have their own names:
* find-the-best-flat-crawler
* find-the-best-flat-extractor
* find-the-best-flat-analysis

The pods are created in find-the-best-flat namespace.

Spec section contains information about:
* containers: name of the container from Azure Container Registry, arguments necessary to run the Python code, like paths to the volumes and used volume mounths
* volumes and their persistent volume claims names
* dnsPolicy is set to "None" and in dnsConfig the nameservers are set to 8.8.8.8 (Google Public DNS) to resolve addresses like www.otodom.pl or overpass-api.de and nominatim.openstreetmap.org. It fixed the problem with "DNS lookup failed: no results for hostname lookup: www.otodom.pl"
* pod restartPolicy is set to Never
