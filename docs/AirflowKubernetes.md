# Deploy Airflow on Kubernetes

Use the idea from: https://bhavaniravi.com/blog/deploying-airflow-on-kubernetes/

Clone the project:

```
git clone https://github.com/bhavaniravi/airflow-kube-setup
```

Adjust volume.yaml in scripts directory to use Azure resources:
```
apiVersion: v1
kind: PersistentVolume
metadata:
  name: airflow-dags
spec:
  capacity:
    storage: 2Gi
  accessModes:
    - ReadWriteMany
  azureFile:
    secretName: azure-account-secret
    shareName: azureairflow/airflowdags
    readOnly: false
  mountOptions:
  - dir_mode=0777
  - file_mode=0777
  - uid=1000
  - gid=1000
  - mfsymlinks
  - nobrl
---
apiVersion: v1
kind: PersistentVolumeClaim
metadata:
  name: airflow-dags
spec:
  accessModes:
    - ReadWriteMany
  storageClassName: ""
  resources:
    requests:
      storage: 2Gi
---
apiVersion: v1
kind: PersistentVolume
metadata:
  name: airflow-logs
spec:
  capacity:
    storage: 2Gi
  accessModes:
    - ReadWriteMany
  azureFile:
    secretName: azure-account-secret
    shareName: azureairflow/airflowlogs
    readOnly: false
  mountOptions:
  - dir_mode=0777
  - file_mode=0777
  - uid=1000
  - gid=1000
  - mfsymlinks
  - nobrl
---
apiVersion: v1
kind: PersistentVolumeClaim
metadata:
  name: airflow-logs
spec:
  accessModes:
    - ReadWriteMany
  storageClassName: ""
  resources:
    requests:
      storage: 2Gi
```

Build the Docker image:

```
cd scripts/docker
docker build -t airflow .
```

Tag the image and push to ACR:
```
docker tag image_name:image_tag ACR_HOST/airflow-example/image_name:image_tag
docker push ACR_HOST/airflow-example/image_name:image_tag
```

Adjust docker image and tag in deploy.sh and run the script:
```
cd scripts/
./kube/deploy.sh -d persistent_mode
```
